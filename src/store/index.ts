import Vue from "vue";
import Vuex from "vuex";

import {
  ADD_MOVING_OBJECT,
  ADD_RHS_OBJECT,
  ADD_LHS_OBJECT,
  OBJECT_DROPPED,
  RIGHT_ARROW,
  LEFT_ARROW
} from './constants';

import {
  MAX_MOMENTUM_DIFF,
  MAX_BENDING
} from '../constants';

import SimulationObject from '../models/SimulationObject';

import {
  calculateObjectsMomentum,
  generateRandomObjectData
} from '../utils';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    movingObject: new SimulationObject(),
    rhsObjects: [],
    lhsObjects: []
  },
  getters: {
    bending(state) {
      const rhsMomentum = calculateObjectsMomentum(state.rhsObjects);
      const lhsMomentum = calculateObjectsMomentum(state.lhsObjects);
      const factor = lhsMomentum >= rhsMomentum ? lhsMomentum : rhsMomentum;

      return (rhsMomentum - lhsMomentum) / 100 * factor;
    },
    hasSimulationEnded(state, getters) {
      const bending = getters.bending;
      const rhsMomentum = calculateObjectsMomentum(state.rhsObjects);
      const lhsMomentum = calculateObjectsMomentum(state.lhsObjects);
      const bendingExceeded = bending > MAX_BENDING || bending < -MAX_BENDING;
      const vastDifference = Math.abs(lhsMomentum - rhsMomentum) > MAX_MOMENTUM_DIFF;

      return bendingExceeded || vastDifference;
    }
  },
  mutations: {
    [ADD_MOVING_OBJECT](state) {
      state.movingObject = generateRandomObjectData();
    },
    [ADD_RHS_OBJECT](state) {
      const randomObject = generateRandomObjectData()
      randomObject.rhs = true;
      state.rhsObjects.push(randomObject);
    },
    [ADD_LHS_OBJECT](state) {
      state.lhsObjects.push(Object.assign({}, state.movingObject));
      state.movingObject = null;
    },
    [RIGHT_ARROW](state) {
      if (state.movingObject.offset - 1 <= 0) return;
      state.movingObject.offset -= 1;
    },
    [LEFT_ARROW](state) {
      if (state.movingObject.offset + 1 > 5) return;
      state.movingObject.offset += 1;
    },
  },
  actions: {
    [OBJECT_DROPPED]({ commit, getters }) {
      commit(ADD_LHS_OBJECT);

      setTimeout(() => {
        if (getters.hasSimulationEnded) {
          alert('Simulation is done');
          location.reload();
          return;
        }
        else {
          commit(ADD_RHS_OBJECT);
        }
      }, 1000);
      setTimeout(() => {
        if (getters.hasSimulationEnded) {
          alert('Simulation is done');
          location.reload();
        } else {
          commit(ADD_MOVING_OBJECT);
        }
      }, 1500);      
    },
  },
  modules: {}
});
