import {
    getWeightColor
} from '../../src/utils';
import {
    WEIGHT_COLORS
} from '../../src/constants';

describe('getWeightUtils', () => {
    it('should return the correct color for even weights', () => {
        const weight1 = 4;
        const weight2 = 8;

        expect(getWeightColor(weight1)).toBe(WEIGHT_COLORS[1]);
        expect(getWeightColor(weight2)).toBe(WEIGHT_COLORS[3]);
    });

    it('should return the correct color for odd weights', () => {
        const weight1 = 3;
        const weight2 = 9;

        expect(getWeightColor(weight1)).toBe(WEIGHT_COLORS[1]);
        expect(getWeightColor(weight2)).toBe(WEIGHT_COLORS[4]);
    });

    it('should return the correct color for corner weights', () => {
        const weight1 = 1;
        const weight2 = 10;

        expect(getWeightColor(weight1)).toBe(WEIGHT_COLORS[0]);
        expect(getWeightColor(weight2)).toBe(WEIGHT_COLORS[4]);
    });

    it('should return the correct color for larger weights', () => {
        const weight = 11;

        expect(getWeightColor(weight)).toBe(WEIGHT_COLORS[4]);
    });
});