export default class SimulationObject {
    type: string;
    rhs?: boolean;
    weight: number;
    offset: number;
}