import {
    MIN_WEIGHT,
    MAX_WEIGHT,
    LEVER_WIDTH,
    OBJECT_TYPES,
    WEIGHT_COLORS,
    OBJECT_TYPES_COUNT
} from './constants';

import SimulationObject from './models/SimulationObject';

export const getWeightColor = (weight: number) => {
    const weightsCount = WEIGHT_COLORS.length;
    const factor = MAX_WEIGHT / weightsCount;
    let colorIndex = Math.ceil(weight / factor) - 1;

    if (colorIndex < 0) {
        colorIndex = 0;
    } else if(colorIndex >= weightsCount) {
        colorIndex = weightsCount - 1;
    }

    return WEIGHT_COLORS[colorIndex];
};

export const calculateObjectsMomentum = (objects) => {
    return objects.reduce((momentum, item) => {
        return momentum += item.weight * item.offset
    }, 0);
}

export const generateRandomObjectData = () => {
    const type = OBJECT_TYPES[Math.floor(Math.random() * OBJECT_TYPES_COUNT)];
    const weight = Math.floor(Math.random() * MAX_WEIGHT) + MIN_WEIGHT;
    const offset = Math.floor(Math.random() * LEVER_WIDTH / 2) + 1;

    const object: SimulationObject = {
        type,
        weight,
        offset
    };

    return object;
};